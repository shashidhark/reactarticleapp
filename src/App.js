import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Article from './components/Article/article';
import User from './components/User/user';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            Links: 
            <Link to='/'>Home</Link> | 
            <Link to='/users'>Users</Link> | 
            <Link to='/articles'>Articles</Link>
            <hr />
            <Route path="/" exact component={()=><div>Welcome to article repo</div>} />
            <Route path="/users" component={User} />
            <Route path="/articles" component={Article} />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
