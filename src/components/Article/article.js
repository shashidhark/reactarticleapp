import React from 'react';
import {articleList} from '../../services/article';
import './article.css';

class Article extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            articleList:[],
            error:null
        };
    }

    componentDidMount(){
        articleList().then((r)=>{
            this.setState({articleList: r.articles});
        }).catch((r)=>{
            this.setState({error:true});
        });
    }   

    viewArticleDetail = (id) => {
        //console.log(this.state.articleList.find((o)=> o.id === id));
        this.setState({articleDetail: this.state.articleList.find((o)=> o.id === id)});
    }

    render(){
        
        let articleUI = "";
        if(this.state.articleList.length>0){
            articleUI = <div className="article">
                {
                    this.state.articleList.map((v, i)=>{
                        return <div key={i} className="title" onClick={()=>this.viewArticleDetail(v.id)}>{v.title}</div>;
                    })
                }
            </div>;
        }
        else{
            articleUI = <div>Loading..</div>
        }

        if(this.state.error){
            articleUI = <div>Error occured!!</div>
        }

        let articleView = "";
        if(this.state.articleDetail){
            let a = this.state.articleDetail;
            articleView = <div className="detail">
                <div className="detailItem"><strong>id:</strong> {a.id}</div>
                <div className="detailItem"><strong>Title:</strong> {a.title}</div>
                <div className="detailItem"><strong>Body:</strong> {a.body}</div>
            </div>
        }

        return (
            <div>
                <div>Click on article</div>
                {articleView}
                {articleUI}
            </div>
        );
    };
}

export default Article;