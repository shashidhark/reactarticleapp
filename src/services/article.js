import axios from '../helper/apiCall';

let articleList = function(){
    return new Promise(function(resolve, reject) {
        axios.get('https://jsonplaceholder.typicode.com/posts')
         .then(function (response) {
            resolve({error:false, articles:response});
        }).catch(()=>{
            reject({error:true, articles:[]});
        });
    });  
};

export {articleList};
